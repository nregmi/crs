/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.kist.crs.controller;

import edu.kist.crs.model.Role;
import edu.kist.crs.repository.RoleRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author LT-64
 */
@RestController
@RequestMapping(value = "/role")
public class RoleController {
    
    @Autowired
    RoleRepository roleRepository;
    
    @RequestMapping(value = "/getAllRoles", method = RequestMethod.GET)
    public List<Role> getAllRoles() {
        return roleRepository.findAll();
    }
}
