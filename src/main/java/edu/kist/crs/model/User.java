/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.kist.crs.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author LT-64
 */
@Entity
@Table(name = "tbl_user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT t FROM User t")
    , @NamedQuery(name = "User.findByUserId", query = "SELECT t FROM User t WHERE t.userId = :userId")
    , @NamedQuery(name = "User.findByUserFname", query = "SELECT t FROM User t WHERE t.userFname = :userFname")
    , @NamedQuery(name = "User.findByUserMname", query = "SELECT t FROM User t WHERE t.userMname = :userMname")
    , @NamedQuery(name = "User.findByUserLname", query = "SELECT t FROM User t WHERE t.userLname = :userLname")
    , @NamedQuery(name = "User.findByUserContactno", query = "SELECT t FROM User t WHERE t.userContactno = :userContactno")
    , @NamedQuery(name = "User.findByUserAddress", query = "SELECT t FROM User t WHERE t.userAddress = :userAddress")
    , @NamedQuery(name = "User.findByUserEmail", query = "SELECT t FROM User t WHERE t.userEmail = :userEmail")
    , @NamedQuery(name = "User.findByUserPassword", query = "SELECT t FROM User t WHERE t.userPassword = :userPassword")})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property="userId")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "user_id")
    private Integer userId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "user_fname")
    private String userFname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "user_mname")
    private String userMname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "user_lname")
    private String userLname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "user_contactno")
    private String userContactno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "user_address")
    private String userAddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "user_email")
    private String userEmail;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "user_password")
    private String userPassword;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tblUserUserId")
    private Collection<Carinfo> tblCarinfoCollection;
    
    @JoinColumn(name = "tbl_role_role_id", referencedColumnName = "role_id")
    @ManyToOne(optional = false)
    private Role tblRoleRoleId;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tblUserUserId")
    private Collection<Rent> tblRentCollection;

    public User() {
    }

    public User(Integer userId) {
        this.userId = userId;
    }

    public User(Integer userId, String userFname, String userMname, String userLname, String userContactno, String userAddress, String userEmail, String userPassword) {
        this.userId = userId;
        this.userFname = userFname;
        this.userMname = userMname;
        this.userLname = userLname;
        this.userContactno = userContactno;
        this.userAddress = userAddress;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserFname() {
        return userFname;
    }

    public void setUserFname(String userFname) {
        this.userFname = userFname;
    }

    public String getUserMname() {
        return userMname;
    }

    public void setUserMname(String userMname) {
        this.userMname = userMname;
    }

    public String getUserLname() {
        return userLname;
    }

    public void setUserLname(String userLname) {
        this.userLname = userLname;
    }

    public String getUserContactno() {
        return userContactno;
    }

    public void setUserContactno(String userContactno) {
        this.userContactno = userContactno;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @XmlTransient
    public Collection<Carinfo> getTblCarinfoCollection() {
        return tblCarinfoCollection;
    }

    public void setTblCarinfoCollection(Collection<Carinfo> tblCarinfoCollection) {
        this.tblCarinfoCollection = tblCarinfoCollection;
    }

    public Role getTblRoleRoleId() {
        return tblRoleRoleId;
    }

    public void setTblRoleRoleId(Role tblRoleRoleId) {
        this.tblRoleRoleId = tblRoleRoleId;
    }

    @XmlTransient
    public Collection<Rent> getTblRentCollection() {
        return tblRentCollection;
    }

    public void setTblRentCollection(Collection<Rent> tblRentCollection) {
        this.tblRentCollection = tblRentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.kist.crs.model.User[ userId=" + userId + " ]";
    }
    
}
