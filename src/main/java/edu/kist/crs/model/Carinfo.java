/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.kist.crs.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author LT-64
 */
@Entity
@Table(name = "tbl_carinfo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Carinfo.findAll", query = "SELECT t FROM Carinfo t")
    , @NamedQuery(name = "Carinfo.findByCarId", query = "SELECT t FROM Carinfo t WHERE t.carId = :carId")
    , @NamedQuery(name = "Carinfo.findByCarName", query = "SELECT t FROM Carinfo t WHERE t.carName = :carName")
    , @NamedQuery(name = "Carinfo.findByCarModel", query = "SELECT t FROM Carinfo t WHERE t.carModel = :carModel")
    , @NamedQuery(name = "Carinfo.findByCarCompany", query = "SELECT t FROM Carinfo t WHERE t.carCompany = :carCompany")
    , @NamedQuery(name = "Carinfo.findByCarNumberplate", query = "SELECT t FROM Carinfo t WHERE t.carNumberplate = :carNumberplate")
    , @NamedQuery(name = "Carinfo.findByCarUserid", query = "SELECT t FROM Carinfo t WHERE t.carUserid = :carUserid")
    , @NamedQuery(name = "Carinfo.findByCarColor", query = "SELECT t FROM Carinfo t WHERE t.carColor = :carColor")
    , @NamedQuery(name = "Carinfo.findByCarPhoto", query = "SELECT t FROM Carinfo t WHERE t.carPhoto = :carPhoto")})
public class Carinfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "car_id")
    private Integer carId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "car_name")
    private String carName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "car_model")
    private String carModel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "car_company")
    private String carCompany;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "car_numberplate")
    private String carNumberplate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "car_userid")
    private int carUserid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "car_color")
    private String carColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "car_photo")
    private String carPhoto;
    @JoinColumn(name = "tbl_user_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false)
    private User tblUserUserId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tblCarinfoCarId")
    private Collection<Rent> tblRentCollection;

    public Carinfo() {
    }

    public Carinfo(Integer carId) {
        this.carId = carId;
    }

    public Carinfo(Integer carId, String carName, String carModel, String carCompany, String carNumberplate, int carUserid, String carColor, String carPhoto) {
        this.carId = carId;
        this.carName = carName;
        this.carModel = carModel;
        this.carCompany = carCompany;
        this.carNumberplate = carNumberplate;
        this.carUserid = carUserid;
        this.carColor = carColor;
        this.carPhoto = carPhoto;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarCompany() {
        return carCompany;
    }

    public void setCarCompany(String carCompany) {
        this.carCompany = carCompany;
    }

    public String getCarNumberplate() {
        return carNumberplate;
    }

    public void setCarNumberplate(String carNumberplate) {
        this.carNumberplate = carNumberplate;
    }

    public int getCarUserid() {
        return carUserid;
    }

    public void setCarUserid(int carUserid) {
        this.carUserid = carUserid;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public String getCarPhoto() {
        return carPhoto;
    }

    public void setCarPhoto(String carPhoto) {
        this.carPhoto = carPhoto;
    }

    public User getTblUserUserId() {
        return tblUserUserId;
    }

    public void setTblUserUserId(User tblUserUserId) {
        this.tblUserUserId = tblUserUserId;
    }

    @XmlTransient
    public Collection<Rent> getTblRentCollection() {
        return tblRentCollection;
    }

    public void setTblRentCollection(Collection<Rent> tblRentCollection) {
        this.tblRentCollection = tblRentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (carId != null ? carId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carinfo)) {
            return false;
        }
        Carinfo other = (Carinfo) object;
        if ((this.carId == null && other.carId != null) || (this.carId != null && !this.carId.equals(other.carId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.kist.crs.model.Carinfo[ carId=" + carId + " ]";
    }
    
}
