/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.kist.crs.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LT-64
 */
@Entity
@Table(name = "tbl_rent")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rent.findAll", query = "SELECT t FROM Rent t")
    , @NamedQuery(name = "Rent.findByRentId", query = "SELECT t FROM Rent t WHERE t.rentId = :rentId")
    , @NamedQuery(name = "Rent.findByRentCarid", query = "SELECT t FROM Rent t WHERE t.rentCarid = :rentCarid")
    , @NamedQuery(name = "Rent.findByRentUserid", query = "SELECT t FROM Rent t WHERE t.rentUserid = :rentUserid")
    , @NamedQuery(name = "Rent.findByRentStartdate", query = "SELECT t FROM Rent t WHERE t.rentStartdate = :rentStartdate")
    , @NamedQuery(name = "Rent.findByRentEnddate", query = "SELECT t FROM Rent t WHERE t.rentEnddate = :rentEnddate")
    , @NamedQuery(name = "Rent.findByRentTotal", query = "SELECT t FROM Rent t WHERE t.rentTotal = :rentTotal")})
public class Rent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "rent_id")
    private Integer rentId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rent_carid")
    private int rentCarid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rent_userid")
    private int rentUserid;
    @Column(name = "rent_startdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date rentStartdate;
    @Column(name = "rent_enddate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date rentEnddate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "rent_total")
    private Double rentTotal;
    @JoinColumn(name = "tbl_carinfo_car_id", referencedColumnName = "car_id")
    @ManyToOne(optional = false)
    private Carinfo tblCarinfoCarId;
    @JoinColumn(name = "tbl_user_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false)
    private User tblUserUserId;

    public Rent() {
    }

    public Rent(Integer rentId) {
        this.rentId = rentId;
    }

    public Rent(Integer rentId, int rentCarid, int rentUserid) {
        this.rentId = rentId;
        this.rentCarid = rentCarid;
        this.rentUserid = rentUserid;
    }

    public Integer getRentId() {
        return rentId;
    }

    public void setRentId(Integer rentId) {
        this.rentId = rentId;
    }

    public int getRentCarid() {
        return rentCarid;
    }

    public void setRentCarid(int rentCarid) {
        this.rentCarid = rentCarid;
    }

    public int getRentUserid() {
        return rentUserid;
    }

    public void setRentUserid(int rentUserid) {
        this.rentUserid = rentUserid;
    }

    public Date getRentStartdate() {
        return rentStartdate;
    }

    public void setRentStartdate(Date rentStartdate) {
        this.rentStartdate = rentStartdate;
    }

    public Date getRentEnddate() {
        return rentEnddate;
    }

    public void setRentEnddate(Date rentEnddate) {
        this.rentEnddate = rentEnddate;
    }

    public Double getRentTotal() {
        return rentTotal;
    }

    public void setRentTotal(Double rentTotal) {
        this.rentTotal = rentTotal;
    }

    public Carinfo getTblCarinfoCarId() {
        return tblCarinfoCarId;
    }

    public void setTblCarinfoCarId(Carinfo tblCarinfoCarId) {
        this.tblCarinfoCarId = tblCarinfoCarId;
    }

    public User getTblUserUserId() {
        return tblUserUserId;
    }

    public void setTblUserUserId(User tblUserUserId) {
        this.tblUserUserId = tblUserUserId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rentId != null ? rentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rent)) {
            return false;
        }
        Rent other = (Rent) object;
        if ((this.rentId == null && other.rentId != null) || (this.rentId != null && !this.rentId.equals(other.rentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.kist.crs.model.Rent[ rentId=" + rentId + " ]";
    }
    
}
